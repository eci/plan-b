# Environmental Cheminformatics Platform: Plan B

This repository contains a collection of ansible scripts which can be
used to provision an apt based GNU/Linux distribution. Currently used
to provision ECI workstations and virtual machines.


## Accessing Resources

When the desktop environment is present on a node (this is the case
for members of the *stations* group of computers), VNC protocol can be
used to access it. Due to particular security setup, only the TigerVNC
viewer can be used to connect to the stations. [available from here
for
Windows](https://bintray.com/tigervnc/stable/download_file?file_path=vncviewer-1.11.0.exe),
or as a package from your GNU/Linux distribution.  For Mac users,
[here is the Java version of the
viewer](https://bintray.com/tigervnc/stable/download_file?file_path=VncViewer-1.11.0.jar).
If you are on Big Sur [try this direct
download](http://tigervnc.bphinz.com/nightly/osx/TigerVNC-1.11.80.dmg).
If you are on Catalina [try this direct
download](https://bintray.com/tigervnc/beta/download_file?file_path=TigerVNC-1.10.90.dmg).
If you are on El Capitan [try this direct
download](https://bintray.com/tigervnc/stable/download_file?file_path=TigerVNC-1.8.0.dmg)



**Before** connecting, you need: 
	- A X509 certificate file (`*.pem`).
	- The IP address of the server.
	- Your username and password.

### Connection instructions

1. Open the VNC viewer.
2. Click `Options`.
3. Switch to `Security` tab.
4. Make sure that at least `TLS with X509 certificates` is checked.
5. Make sure that at least `None` is checked in the `Authentication` section.
6. Set the `Path to X509 certificate` to the location of the X509
   certificate file.
7. Now, click `OK` to accept changes and close the `Options` tab.
8. Fill in the IP address of the server in field `VNC server` and
   append `:5950` to it. So, the `VNC server` entry should look like
   `XXX.XXX.XXX.XXX:5950`, or `example.somewhere.biz`.
9. Click `Connect`.
10. **When at the login screen, ensure you are using the `Xfce Session` (settings wheel, bottom right) before entering your password**.
11. Pro-tip: User experience may be better in fullscreen (`F8` and select `fullscreen`)

## Using Environmental Cheminformatics programs

### Getting started with Shinyscreen

Shinyscreen is ECI's mass spectrum inspection program. It was written
in R and exists as a R package. In order to run it, it is necessary to
start a R session. 

While most students get to know R through R Studio, this development
environment (DE) is by no means the only one that can run R (indeed,
you do not even need a DE to run R: try it yourself, start a command
console and type `R` in there). The default choice of R DE on
`alien-one` currently is GNU Emacs with its *Emacs Speaks Statistics
(ESS)* mode.

Steps:

1. Fire up emacs
2. `Alt-x` (`Cmd-x` for Mac) and `R` gets you an R session
3. If asked, set your start working directory, e.g. 
`setwd('/mnt/scratch/user/')` 
(where `user` is an alias for your username)
4. Enter `library(shinyscreen)`
5. Enter `options(browser="chromium")`
6. Enter `launch()` 

To create subdirectories for different Shinyscreen calculations, 
use `dir.create()` and `setwd()` to create and set new working 
directories, respectively. This is highly recommended otherwise 
you will auto-detect and/or overwrite previous results. 


Alternatively, there there are two icons on your desktop that will
start Shinyscreen immediately without a R session. One starts the
~~stable~~outdated version of Shinyscreen. The other runs a
development version (~~you can try it out if you feel
adventurous~~recommended).

### Some emacs tips
To execute a script in Emacs Ess, you have different shortcuts:

1. `Ctrl alt x`: runs a Paragraph
2. `Ctrl enter`: similar, but closer to Rstudio in how it 
treats collections of sratements
3. `Ctrl j`: runs a single line

The above commands are used when you have a separate R script that is 
being evaluated in the session. In the session itself, it's just enter.

Some venerable Emacs personalities suggest window-based organisation
of Emacs workflow:

1. Start Emacs.
2. Resize it to a strip at the bottom of the screen (the *session*
   window), then `Alt-x R`.
3. C-x 5 2 to open a new window (connected to the same session).
4. Open an existing R script, or start writing a new one.
5. Commands from above will work and be displayed in the session window.
6. Switch between the windows with `C-x 5 o`.
7. You can also split windows in panes with `C-x 2`,`C-x 3`, hide all
   but one with `C-x 1`, hide this one `C-x 0`, ...
8. Reading the `Emacs Tutorial` is not a bad place to start if you
   want to delve deeper [note that ECI's Emacs is heavily customised,
   so it might not behave as you'd expect].

## Ansible System

Host file lists ECI computational inventory. There are two types of
items, *stations* -- the compute platforms intended to be used by all
ECI members and *ctrl* -- the control nodes intended to provision
*stations* without too much involvement by human beings.

#!/bin/env bash
export GUIX_PROFILE=$HOME/.pcl-guix-prof
. $GUIX_PROFILE/etc/profile

cd "$HOME/pcl"
pubchem/pubchemlite/pb-lite-driver.sh share/CompoundDBs/PubChem/PubChemLite/input

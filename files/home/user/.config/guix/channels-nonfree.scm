(define channels
 ;; This is custom Environmental Cheminformatics LCSB channel.
 (list (channel
        (name 'eci-addons)
        (url "https://git.sr.ht/~condor/eci-guix")
        (branch "master")
        (introduction
         (make-channel-introduction
          "6642600059913e86657242ba369d752c0af2bff5"
          (openpgp-fingerprint
           "EDC3 9790 A06D 5F72 41D9  70F9 BBEF E71B 7A28 3C6F"))))
       (channel
         (name 'guix)
         (url "https://git.savannah.gnu.org/git/guix.git")
	 (commit "10b909a0249fd53d589890b357232db4165690f5")
	 ;; (commit "2b5a81dfd3f7a633f4137d31060d0fa82c5e89f6")
         ;; (commit "66c31d5072c846ab86026c5f696ea55a08723ac")
	 
	 )
       (channel
	(name 'nonguix)
	(url "https://gitlab.com/nonguix/nonguix")
	(introduction
	 (make-channel-introduction
	  "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
	  (openpgp-fingerprint
	   "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))))

channels

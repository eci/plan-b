(define channels
 ;; This is custom Environmental Cheminformatics LCSB channel.
 (list (channel
        (name 'eci-addons)
        (url "https://git.sr.ht/~condor/eci-guix")
        (branch "master")
        (introduction
         (make-channel-introduction
          "6642600059913e86657242ba369d752c0af2bff5"
          (openpgp-fingerprint
           "EDC3 9790 A06D 5F72 41D9  70F9 BBEF E71B 7A28 3C6F"))))
       (channel
         (name 'guix)
         (url "https://git.savannah.gnu.org/git/guix.git")
	 (commit "10b909a0249fd53d589890b357232db4165690f5"))))

channels

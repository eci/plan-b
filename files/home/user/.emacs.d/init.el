;;; Commentary:
;;;
;;; This is the top-level /default/ Emacs configuration file. Below
;;; are definitions of the package repositories needed for basic
;;; functionality and then functions to load the definitions contained
;;; in an org mode file.


(defvar eci-init "~/.emacs.d/eci-init.el")
(setq custom-file "~/.emacs.d/custom.el")
(if (file-exists-p eci-init) (load eci-init) nil)
(if (file-exists-p custom-file) (load custom-file) nil)
